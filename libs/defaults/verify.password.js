module.exports = function(data){

	return new Promise((resolve,reject)=>{

		if(data.password){

			let pass = this.App.verifyPassword(data.password);
			if(pass.strong) {
				this.$state.socket.emit(this.name,{ok:true});
			} else {
				this.$state.socket.emit(this.name,{ok:false});
			}

		}

	});

};