export default class State {
		
	constructor(socket,options){

		try {

			this.socket = socket;

			this.user = {

				auth:null,
				host:false,
				secret:false,

			};

			this.stores = {
				lastComment:0
			}; // store variables for this.$state here

			this.intervals = [];
			this.timeouts = [];
			this.files = {}; // file uploads

			// clean timers on disconnect
			this.socket.on('disconnect',()=>{

				if(this.intervals.length>0){

					this.intervals.forEach(interval=>{

						clearInterval(interval);

					});
				}		

				if(this.timeouts.length>0){

					this.timeouts.forEach(timeout=>{

						clearTimeout(timeout);

					});
				}

			});

		} catch (e) {

			throw e;

		}

	}



};