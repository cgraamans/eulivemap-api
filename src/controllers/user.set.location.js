module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let tags = {};
			let err = false;
			let that = this;

			if(data.lat && data.lon && (typeof data.lon === 'number' && typeof data.lat === 'number')) tags.coords = [data.lon,data.lat];
			if(data.country && data.country.length<3 && typeof data.country === 'string') tags.country = data.country;

			// if(!data.text || data.text.length < 1) err = 'empty comment';
			if(!this.$state.user.auth || !this.$state.user.auth.id) err = 'not logged in';
			
			if(!err) {

				let pOors = [];
				if(tags.coords) {

					pOors.push(new Promise((res,rej)=>{

						that.App.DB.q('UPDATE user_locations SET longitude = ?, latitude = ? WHERE user_id = ?',[tags.coords[0],tags.coords[1],that.$state.user.auth.id])
							.then(()=>{
								res();
							})
							.catch(e=>{
								rej(e);
							});

					}));

				}

				if(tags.country) {

					pOors.push(new Promise((res,rej)=>{

						that.App.DB.q('SELECT countrycode FROM data_countries WHERE countrycode = ?',[tags.country])
						.then(r=>{
							if(r.length === 1) {
		
								that.App.DB.q('UPDATE user_locations SET countrycode = ? WHERE user_id = ?',[tags.country,that.$state.user.auth.id])
									.then(()=>{
										res();
									})
									.catch(e=>{
										rej(e);
									});

							} else {
								res();
							}
						})
						.catch(e=>{
							rej(e);
						});					

					}));

				}

				if(pOors.length > 0) {

					Promise.all(pOors)
						.then(()=>{

							this.$state.socket.emit(this.name,{ok:true});
							resolve();

						})
						.catch(e=>{

							throw e;
						
						});

				} else {

					resolve();

				}

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});	
				resolve();

			}

		} catch(e){
			
			this.$state.socket.emit(this.name,{ok:false,e:e,msg:'runtime error'});				
			console.log('runtime error',e);

			resolve();

		}

	});

};
