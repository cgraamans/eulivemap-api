module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let err = false;
			// if(!this.$state.user.auth || !this.$state.user.auth.id) err = 'Not logged in.';
			let lookUp = {};
			if(data.all) lookUp.all = true;
			if(data.country && typeof data.country === 'string' && data.country.length < 4) lookUp.country = data.country;
			if(data.sortBy && typeof data.sortBy === 'string' && ['countrycode','name','capital','continent'].includes(data.sortBy)) lookUp.sortBy = data.sortBy;
			if(data.sortDir && typeof data.sortDir === 'string' && ['ASC','DESC'].includes(data.sortDir.toUpperCase())) lookUp.sortDir = data.sortDir.toUpperCase();
			if(!err) {

				// model
				let mDataClass = require('../models/data').default;
				let mData = new mDataClass(this.App);

				mData.getCountryData(lookUp)
					.then(res=>{

						this.$state.socket.emit(this.name,{ok:true,results:res});
						resolve();

					})
					.catch(e=>{

						throw e;

					})

				resolve();

			} else {

				throw err;

			}

		} catch(e) {

			console.error('runtime error',e);
			this.$state.socket.emit(this.name+'.done',{ok:false,msg:'runtime error',e:e});
			resolve();

		}


	});

};
