module.exports = function(data){

	return new Promise((resolve,reject)=>{

		try {

			let err;
			if(!data.stub) err = 'missing comment stub';
			if(!data.dir) {
				data.dir = 0;
			} else {
				data.dir = 1;
			}

			if(!this.$state.user || !this.$state.user.auth || this.$state.user.auth.auth < 1) err = 'not logged in';

			if(!err) {

				this.App.DB.q('UPDATE item_comments SET isRemoved = ?, dt_delete = ? WHERE stub = ?',[
						data.dir,
						Math.round((new Date()).getTime()/1000),
						data.stub
					])
					.then(resultOfUpdate=>{

						if(resultOfUpdate.affectedRows > 0) {

							let modelCommentsClass = require('../models/comments').default;
							let mComments = new modelCommentsClass(this.App);

							mComments.getComments({
									stub:data.stub,
									userId:this.$state.user.auth.id,
									userName:this.$state.user.auth.name
								})
								.then(comments=>{

									if(comments.length === 1) this.$state.socket.emit(this.name,{ok:true,result:comments[0]});
									resolve();

								})
								.catch(e=>{

									throw e;

								});

						} else {

							resolve();

						}
					})
					.catch(e=>{

						throw e

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});
				resolve();

			}

		} catch (e) {

			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();

		}

	});

};