module.exports = function(data){

	return new Promise((resolve,reject)=>{

		try {

			let err;
			if(!data.name) err = 'missing user name';
			if(!data.stub) err = 'missing comment stub'
			if(!this.$state.user || !this.$state.user.auth || this.$state.user.auth.auth < 2 ) err = 'not logged in';
			if(!data.active) data.active = 0;
			if(data.active) data.active = 1;

			if(!err) {

				this.App.DB.q('SELECT id FROM users WHERE name = ?',[data.name])
					.then(idArr=>{
						if(idArr.length>0) {

							let idOfItem = idArr[0].id;
							if(idOfItem !== this.$state.user.auth.id) {

								this.App.DB.q('INSERT INTO `user_bans` SET ?',{
										user_id:idOfItem,
										reason:'comment: '+data.stub,
										dt:Math.round((new Date()).getTime()/1000),
										by_admin:this.$state.user.auth.id,
										active:data.active
									})
									.then(()=>{

										this.$state.socket.emit(this.name,{ok:true,result:{
											name:data.name,
											active:data.active
										}});
										resolve();

									})
									.catch(e=>{

										throw e

									});

							} else {

								this.$state.socket.emit(this.name,{ok:false,msg:'you cannot ban yourself'});
								resolve();

							}

						} else {

							this.$state.socket.emit(this.name,{ok:false,msg:'missing user'});
							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});
				resolve();

			}

		} catch (e) {

			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();

		}

	});

};