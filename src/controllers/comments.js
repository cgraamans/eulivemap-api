module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let err = null;

			let modelCommentsClass = require('../models/comments').default;
			let mComments = new modelCommentsClass(this.App);

			let runObj = {};

			if(data.sortBy) runObj.sortBy = data.sortBy;
			if(this.$state.user.auth && this.$state.user.auth.id && this.$state.user.auth.name) {
				runObj.userId = this.$state.user.auth.id;
				runObj.userName = this.$state.user.auth.name;
			} 
			if(data.upTo && typeof data.upTo === 'string') runObj.upTo = data.upTo;
			if(data.parent && typeof data.parent === 'string') runObj.parent = data.parent;
			if(data.stub && typeof data.stub === 'string') runObj.stub = data.stub;

			!data.item ? err = 'No item.' : runObj.item = data.item;

			if(!err) {

				mComments.getComments(runObj)
					.then(comments=>{

						this.$state.socket.emit(this.name,{ok:true,results:comments});
						resolve();

					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});
				resolve();

			}

		} catch(e){

			console.error('runtime error',e);
			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();
		}

	});

};
