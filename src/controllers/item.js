module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let modelItemClass = require('../models/items').default;
			let modelItems = new modelItemClass(this.App.DB);
			
			let modelItemLocationClass = require('../models/item-locations').default;
			let modelLocations = new modelItemLocationClass(this.App.DB);
			let dataObj = {};

			if(data.stub && typeof data.stub === 'string') {

				dataObj.item = data.stub;

				let uId = false;
				if(this.$state.user && this.$state.user.auth) uId = this.$state.user.auth.id;

				modelItems.fetch(dataObj,uId)
					.then(results=>{

						if(results.length === 1) {
							this.$state.socket.emit(this.name,{ok:true,results:results});
						} else {
							this.$state.socket.emit(this.name,{ok:false,e:'item not found.'});
						}
						resolve(data);

					})
					.catch(e=>{

						this.$state.socket.emit(this.name,{ok:false,e:e});				
						console.log(e);
						resolve(data);
					
					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,e:'missing stub'});				
				resolve(data);

			}

		} catch(e){

			console.log(e);
			resolve(data);

		}

	});

};
