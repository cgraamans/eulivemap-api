
/*

	items controller

	from		- stub (up)
	upTo		- stub (down)
	offset		- time offset
	source		- source stub
	sortBy		- new,hot,old,comments
	sortDir		- asc, desc
	type 		- O, N, NT, T, NP
	limit		- 20
	language	- language code (2 chars)
	country		- country (2 chars)

*/

module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let modelItemClass = require('../models/items').default;
			let modelItems = new modelItemClass(this.App.DB);
			
			let modelItemLocationClass = require('../models/item-locations').default;
			let modelLocations = new modelItemLocationClass(this.App.DB);

			let now = Math.floor((new Date()).getTime()/1000);
			
			let dataObj = {
				offset: now - 604800,
				limit:20,
			};

			let valid = {
				sortBy:['new','hot','old','comments'],
				type:['O','N','T','M'],
				sourceType:['twitter','rss','users']
			}

			// Maximum time to show from
			if(data.offset && typeof data.offset === 'number') data.offset > 2592000 ? now - 2592000 : dataObj.offset = now - data.offset;

			// validation
			if(data.upTo && typeof data.upTo === 'string') dataObj.upTo = data.upTo;
			if(data.from && typeof data.from === 'string') dataObj.from = data.from;
			if(data.source && typeof data.source === 'string') dataObj.source = data.source;
			if(data.item && typeof data.item === 'string') dataObj.item = data.item;
			if(data.stub && typeof data.stub === 'string') dataObj.stub = data.stub;
			if(data.country && data.country.length === 2 && typeof data.country === 'string') dataObj.country = data.country.toUpperCase();
			if(data.language && data.language.length === 2 && typeof data.language === 'string') dataObj.language = data.language.toUpperCase();
			if(data.sortBy && valid.sortBy.includes(data.sortBy.toLowerCase())) dataObj.sortBy = data.sortBy.toLowerCase();
			if(data.type && valid.type.includes(data.type.toUpperCase())) dataObj.type = data.type.toUpperCase();

			// if(data.types && data.types.length > 0) {
			// 	data.types.forEach((type,id)=>{
			// 		 if(!['O','N','T','S'].includes(type.toUpperCase())) data.types[id] = 'N'
			// 	});
			// 	dataObj.type = data.types.filter(function(item, pos, self) {
			// 		return self.indexOf(item) == pos;
			// 	});
			// }
			
			if(data.limit && typeof data.limit === 'number') dataObj.limit = data.limit;
			if(data.sourceType && valid.sourceType.includes(data.sourceType.toLowerCase())) dataObj.sourceType = data.sourceType.toLowerCase();

			let uId = false;
			if(this.$state.user && this.$state.user.auth) uId = this.$state.user.auth.id;

			modelItems.fetch(dataObj,uId)
				.then(results=>{

					this.$state.socket.emit(this.name,{ok:true,results:results});
					resolve(data);

				})
				.catch(e=>{

					this.$state.socket.emit(this.name,{ok:false,e:e,msg:'runtime error'});				
					console.log(e);
					resolve(data);
									
				});

		} catch(e){

			console.log(e);
			resolve(data);

		}

	});

};
