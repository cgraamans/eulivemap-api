module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let that = this;

			let modelCommentsClass = require('../models/comments').default;
			let modelComments = new modelCommentsClass(this.App);
			let striptags = require('striptags');

			let err = false;

			if(data.text) data.text = striptags(data.text);
			if(!data.text || data.text.length < 1) err = 'empty comment';
			if(!data.item) err = 'no item';

			if(!this.$state.user.auth) err = 'not logged in.';

			if(!err) {

				let icMax = (new Date()).getTime() - this.App.options.APP.commentIntervalMax;
				if(this.$state.stores.lastComment > icMax) {

					let s = Math.round((this.$state.stores.lastComment - icMax)/1000);
					this.$state.socket.emit(this.name,{ok:false,msg:'Posting timeout. Wait '+s+' s before trying again'});
					resolve();

				} else {

					let returnComment = function(finalCommentObj){

						if(finalCommentObj.length>0) {
							that.$state.socket.emit(that.name,{ok:true,res:finalCommentObj[0]});				
						} else {
							that.$state.socket.emit(that.name,{ok:false,msg:'Error retrieving comment after insert/update'});
						}
						resolve();

					};

					let isMutation = false;
					this.$state.stores.lastComment = (new Date()).getTime();
					if(data.stub) {

						isMutation = true;
						modelComments.setText({
								stub:data.stub,
								text:data.text
							})
							.then(resOfSetText=>{
								
								if(!resOfSetText) {

									modelComments.getComments({
											stub:data.stub,
											userId:this.$state.user.auth.id,
											userName:this.$state.user.auth.name
										})
										.then(finalCommentObj=>{

											returnComment(finalCommentObj);

										})
										.catch(e=>{

											throw e;

										});
								
								} else {

									this.$state.socket.emit(this.name,{ok:false,msg:resOfSetText});	

								}


							})
							.catch(e=>{

								throw e;

							});

					} else {

						let parent = null;
						let opts = {
							item:data.item,
							text:data.text,
							userId:this.$state.user.auth.id,
						};
						if(data.parent) opts.parent = data.parent;

						modelComments.setComment(opts)
							.then(commentId=>{

								modelComments.getComments({id:commentId})
									.then(finalCommentObj=>{

										returnComment(finalCommentObj);

									})
									.catch(e=>{

										throw e;

									});

							}).catch(e=>{

								throw e;

							});

					}

				}

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});	
				resolve();

			}				

		} catch(e){
			
			this.$state.socket.emit(this.name,{ok:false,e:e,msg:'runtime error'});				
			console.log('runtime error',e);

			resolve();

		}

	});

};
