module.exports = function (data) {

	return new Promise((resolve,reject)=>{


		// console.log(this.name);
		// console.log('example data:',data);
		// console.log('env:',this);
		// console.log(data);
		if(!data.from) data.from = 2592000
		
		let sqlStr = `SELECT il.id, il.lat, il.lon, i.stub, dc.lat as source_lat, dc.lon as source_lon, s.logo as source_logo, i.text
					FROM items AS i
					INNER JOIN sources s ON s.id = i.source_id
					INNER JOIN \`data_countries\` dc ON s.country = dc.countrycode
					LEFT JOIN \`item_locations\` il ON il.item_id = i.id
					WHERE i.dt_created > ?
			`,
			sqlWhereArr = [Math.floor((new Date()).getTime()/1000) - data.from];

		if(data.stub) {
			sqlStr += `AND i.stub = ?`;
			sqlWhereArr.push(data.stub);
		}

		if(data.stubs) {
			sqlStr += ' AND ( ';
			for(let i = 0,c=data.stubs.length;i<c;i++) {
				
				sqlStr += `
							i.stub = ?
						`;
				if(i<c-1) sqlStr+=' OR ';
			}
			sqlStr += ')';
			sqlWhereArr = sqlWhereArr.concat(data.stubs);
		}

		this.App.DB.q(sqlStr,sqlWhereArr)
			.then(res=>{
				this.$state.socket.emit(this.name,{ok:true,results:res});
			})
			.catch(e=>{
				this.$state.socket.emit(this.name,{ok:false,e:e});
			});
			
		resolve(data);

	});

};
