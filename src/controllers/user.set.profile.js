module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let striptags = require('striptags');

			let err = false;

			if(data.text) data.text = striptags(data.text);
			if(!data.text || data.text.length < 1) err = 'empty comment';
			if(!this.$state.user.auth || !this.$state.user.auth.id) err = 'not logged in';

			if(!err) {

				this.App.DB.q('UPDATE users SET profile = ? WHERE id = ?',[data.text,this.$state.user.auth.id])
					.then(()=>{

						this.$state.socket.emit(this.name,{ok:true});				
						resolve();

					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});	
				resolve();
			}

		} catch(e){
			
			this.$state.socket.emit(this.name,{ok:false,e:e,msg:'runtime error'});				
			resolve();

		}

	});

};
