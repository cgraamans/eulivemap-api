module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let opts = null;

			if(this.$state.user.auth.id) opts = {id:this.$state.user.auth.id};

			if(opts) {

				// model
				let mUserClass = require('../models/users').default;
				let mUser = new mUserClass(this.App);

				mUser.getUserData(opts)
					.then(resOfUD=>{
						
						let r = {ok:true,result:resOfUD};
						this.$state.socket.emit(this.name,r);
						resolve();

					})
					.catch(e=>{

						throw e;

					})

			} else {

				resolve();

			}

		} catch(e) {

			console.error('runtime error',e);
			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();

		}


	});

};
