module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {


			// req libs
			let sharp = require('sharp');
			let fs = require('fs');

			// model
			let mUserClass = require('../models/users').default;
			let mUser = new mUserClass(this.App);

			// Start Code
			let err = false;

			if(!data.fileData) err = 'Missing fileData';
			if(data.fileData && data.fileData.type && !['image/jpeg','image/png'].includes(data.fileData.type)) err = 'Bad file type.';
			if(!this.$state.user || !this.$state.user.auth || !this.$state.user.auth.id) 'Not logged in.'

			if(!err) {

				if(!this.$state.files[data.fileData.name]) {

					this.$state.files[data.fileData.name] = Object.assign({ 
						name: null, 
						type: null, 
						size: 0, 
						data: [], 
						slice: 0, 
					},data.fileData);
					this.$state.files[data.fileData.name].data = [];

				}

				let cleanedData = data.fileData.data.replace("data:application/octet-stream;base64,", "");

				this.$state.files[data.fileData.name].data.push(new Buffer(cleanedData,'base64'));
				this.$state.files[data.fileData.name].slice++;

				if (this.$state.files[data.fileData.name].slice * 100000 >= this.$state.files[data.fileData.name].size) {
					
					mUser.getAvatarStub(this.$state.user.auth.id)
						.then(stub=>{

							let fB = Buffer.concat(this.$state.files[data.fileData.name].data);
							if(fB.length > 0 && Buffer.isBuffer(fB)) {

								let pArr = [

									new Promise((res,rej)=>{

										sharp(fB)
											.resize(256,256,{fit:'inside'})
											.png()
											.toBuffer()
											.then(sharpData=>{

												fs.writeFile(__dirname+'/../../assets/avatars/'+stub+'.png', sharpData, (err)=>{
												    if (err) throw 'error writing file: ' + err;
												    res();
												});

											})
											.catch(e=>{
												console.log('sharp error',e);
												rej(e);
											});

									}),

									new Promise((res,rej)=>{

										sharp(fB)
											.resize(36,36,{fit:'inside'})
											.png()
											.toBuffer()
											.then(sharpData=>{

												sharp(__dirname+'/../../assets/avatars/template.png')
													.overlayWith(sharpData)
													.png()
													.toBuffer()
													.then(sharpFinal=>{
		
														fs.writeFile(__dirname+'/../../assets/avatars/'+stub+'.btn.png', sharpFinal	, (err)=>{
														    if (err) throw 'error writing file: ' + err;
														    res();
														});

													})
													.catch(e=>{
														console.log('sharp error',e);
														rej(e);
													});

											})
											.catch(e=>{
												console.log('sharp error',e);
												rej(e);
											});
									}),
								];

								Promise.all(pArr)
									.then(()=>{

										delete this.$state.files[data.fileData.name];
										this.$state.socket.emit(this.name+'.done',{ok:true});
										resolve();

									})
									.catch(e=>{

										delete this.$state.files[data.fileData.name];

										throw e;
									});

						} else {

							throw e;
						
						}

					})
					.catch(e=>{

						throw e;

					});

				} else { 

					// get slice
					this.$state.socket.emit('user.avatar.set.slice', { 
						currentSlice: this.$state.files[data.fileData.name].slice 
					}); 
					resolve();

				}


			} else {

				this.$state.socket.emit(this.name+'.done',{ok:false,msg:err});
				resolve();

			}


		} catch(e) {

			console.error('runtime error',e);
			this.$state.socket.emit(this.name+'.done',{ok:false,msg:'runtime error',e:e});
			resolve();
		}
		
	});

};
