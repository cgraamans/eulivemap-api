module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			if(this.$state.user.auth.id){

				this.App.DB.q('INSERT INTO user_locations SET ?',{
						user_id:this.$state.user.auth.id,
						countrycode:'BE',
						dt:Math.floor((new Date()).getTime()/1000)
					})
					.then(()=>{

						resolve();

					})
					.catch(e=>{

						throw e;

					});
			
			}

		} catch(e){
			
			this.$state.socket.emit(this.name,{ok:false,e:e,msg:'runtime error'});				
			resolve();

		}

	});

};
