module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let err = null;

			let modelCommentsClass = require('../models/comments').default;
			let mComments = new modelCommentsClass(this.App);

			let runObj = {};

			if(data.item && typeof data.item === 'string') {
				runObj.item = data.item;
			} else {
				err = 'No item.';
			}

			console.log(data,runObj,err);

			if(!err) {

				mComments.getLocations(runObj)
					.then(comments=>{

						console.log(comments);

						this.$state.socket.emit(this.name,{ok:true,results:comments});
						resolve();

					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});
				resolve();

			}

		} catch(e){

			console.error('runtime error',e);
			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();
		}

	});

};
