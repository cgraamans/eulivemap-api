module.exports = function(data){

	return new Promise((resolve,reject)=>{

		try {

			let err;
			if(!data.stub) err = 'missing item stub';
			(data.type && typeof data.type === 'number' && data.type < 3) ? false : data.type = 0;

			if(!this.$state.user || !this.$state.user.auth) err = 'not logged in';

			if(!err) {

				this.App.DB.q('SELECT id FROM items WHERE stub = ?',[data.stub])
					.then(idArr=>{
						if(idArr.length>0) {

							let idOfItem = idArr[0].id;

							this.App.DB.q('REPLACE INTO `item_reports` (`item_id`,`user_id`, `type`) VALUES (?,?,?)',[idOfItem,this.$state.user.auth.id,data.type])
								.then(()=>{

									this.$state.socket.emit(this.name,{ok:true,stub:data.stub,type:data.type});
									resolve();

								})
								.catch(e=>{

									throw e

								});

						} else {

							this.$state.socket.emit(this.name,{ok:false,msg:'missing item'});
							resolve();

						}
					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});
				resolve();

			}

		} catch (e) {

			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();

		}

	});

};