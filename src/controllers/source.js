module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let modelSourceClass = require('../models/sources').default;
			let modelSources = new modelSourceClass(this.App.DB);

			let err = false;
			let uId = false;

			let transData = {};

			if(data.stub) transData.stub = data.stub;
			if(this.$state.user && this.$state.user.auth) uId = this.$state.user.auth.id;
			modelSources.fetch(transData,uId)
				.then(results=>{

					if(results.length> 0) {

						this.$state.socket.emit(this.name,{ok:true,results:results});
					
					} else {
					
						this.$state.socket.emit(this.name,{ok:false,e:'source not found.'});

					}
					resolve(data);

				})
				.catch(e=>{

					throw e;

				});

		} catch(e){

			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			console.log(e);
			resolve();

		}

	});

};