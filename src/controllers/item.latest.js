module.exports = function(data){

	return new Promise((resolve,reject)=>{

		try {

			let err = false;
			let optSHOWITEMS = false;
			let rtn = {
				ok:false,
			};

			if (!data.stub || typeof data.stub !== 'string') err = 'Needs a stub'
			if (data.getItems) optSHOWITEMS = true;
			
			if(!err) {

				this.App.DB.q(`
						SELECT 
							COUNT(*) AS cnt 
						FROM items 
						WHERE 
							dt_created > (SELECT ii.dt_created FROM items AS ii WHERE stub = ?)
					`,[data.stub])
					.then(counter=>{
						
						if(counter.length>0) {

							rtn.count = counter[0].cnt;

							if(optSHOWITEMS) {

								let mItemsClass = require('../models/items').default;
								let mItems = new mItemsClass(this.App);
								let uId = false;

								if(this.$state.user && this.$state.user.auth && this.$state.user.auth.id) uId = this.$state.user.auth.id;

								let optForFetch = {
									sortBy:'new',
									from:data.stub,
								}
								mItems.fetch(optForFetch,uId)
									.then(items=>{

										rtn.items = items;
										rtn.ok = true;
										this.$state.socket.emit(this.name,rtn);
										resolve();

									})
									.catch(e=>{

										throw e;
									
									});


							} else {

								rtn.ok = true;
								this.$state.socket.emit(this.name,rtn);
								resolve();

							}

						} else {
			
							this.$state.socket.emit(this.name,Object.assign({},rtn,{msg:'item not found'}));
							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,Object.assign({},rtn,{msg:err}));
				resolve();

			}

		} catch(e) {

			this.$state.socket.emit(this.name,Object.assign({},rtn,{msg:'runtime error',e:e}));
			console.log('runtime error',e)
			resolve();

		}

	});

};