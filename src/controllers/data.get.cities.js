module.exports = function (data) {

	return new Promise((resolve,reject)=>{

		try {

			let err = false;
			// if(!this.$state.user.auth || !this.$state.user.auth.id) err = 'Not logged in.';
			let lookUp = {};
			if(data.name && typeof data.name === 'string' && data.country.length < 128) lookUp.name = data.name;
			if(data.country && typeof data.country === 'string' && data.country.length < 4) lookUp.country = data.country;
			if(data.sortBy && typeof data.sortBy === 'string' && ['pop','alt','name'].includes(data.sortBy)) lookUp.sortBy = data.sortBy;
			if(data.sortDir && typeof data.sortBy === 'string' && ['ASC','DESC'].includes(data.sortBy.toUpperCase())) lookUp.sortBy = data.sortBy.toUpperCase();
			if(!err) {

				// model
				let mDataClass = require('../models/data').default;
				let mData = new mDataClass(this.App);

				mData.getCityData(lookUp)
					.then(res=>{

						this.$state.socket.emit(this.name,{ok:true,results:res});
						resolve();

					})
					.catch(e=>{

						throw e;

					})

				resolve();

			} else {

				throw err;

			}

		} catch(e) {

			console.error('runtime error',e);
			this.$state.socket.emit(this.name+'.done',{ok:false,msg:'runtime error',e:e});
			resolve();

		}


	});

};
