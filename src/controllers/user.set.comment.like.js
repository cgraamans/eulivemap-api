module.exports = function(data){

	return new Promise((resolve,reject)=>{

		let err;
		if(!data.stub) err = 'missing item stub';
		(data.like) ? data.like = 1 : data.like = 0;

		if(!this.$state.user || !this.$state.user.auth) err = 'not logged in';

		if(!err) {

			this.App.DB.q('SELECT id FROM item_comments WHERE stub = ?',[data.stub])
				.then(idArr=>{
					if(idArr.length>0) {

						let idOfItem = idArr[0].id;

						this.App.DB.q('REPLACE INTO `item_comment_likes` (`comment_id`,`user_id`, `liked`) VALUES (?,?,?)',[idOfItem,this.$state.user.auth.id,data.like])
							.then(()=>{

								this.$state.socket.emit(this.name,{ok:true,stub:data.stub,like:data.like});

							})
							.catch(e=>{

								this.$state.socket.emit(this.name,{ok:false,msg:'item.like failed',e:e});

							});

					} else {
						this.$state.socket.emit(this.name,{ok:false,msg:'missing comment'});
					}
				})
				.catch(e=>{

					this.$state.socket.emit(this.name,{ok:false,msg:e});

				});

		} else {

			this.$state.socket.emit(this.name,{ok:false,msg:err});
		
		}
		resolve(data);

	});

};