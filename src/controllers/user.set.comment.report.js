module.exports = function(data){

	return new Promise((resolve,reject)=>{

		try {

			let err;
			if(!data.stub) err = 'missing comment stub';
			(data.type && typeof data.type === 'number') ? false : data.type = 0;

			if(!this.$state.user || !this.$state.user.auth) err = 'not logged in';

			if(!err) {

				this.App.DB.q('SELECT id FROM item_comments WHERE stub = ?',[data.stub])
					.then(idArr=>{
						if(idArr.length>0) {

							let idOfItem = idArr[0].id;

							this.App.DB.q('REPLACE INTO `item_comment_reports` (`comment_id`,`user_id`, `type`) VALUES (?,?,?)',[idOfItem,this.$state.user.auth.id,data.type])
								.then(()=>{

									let modelCommentsClass = require('../models/comments').default;
									let mComments = new modelCommentsClass(this.App);

									mComments.getComments({
											stub:data.stub,
											userId:this.$state.user.auth.id,
											userName:this.$state.user.auth.name
										})
										.then(comments=>{

											if(comments.length === 1) this.$state.socket.emit(this.name,{ok:true,result:comments[0]});
											resolve();

										})
										.catch(e=>{

											throw e;

										});

								})
								.catch(e=>{

									throw e

								});

						} else {

							this.$state.socket.emit(this.name,{ok:false,msg:'missing comment'});
							resolve();

						}
					})
					.catch(e=>{

						throw e;

					});

			} else {

				this.$state.socket.emit(this.name,{ok:false,msg:err});
				resolve();

			}

		} catch (e) {

			this.$state.socket.emit(this.name,{ok:false,msg:'runtime error',e:e});
			resolve();

		}

	});

};