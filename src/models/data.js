export default class Model {

	constructor(App){

		try {
		
			this.App = App;


		} catch(e) {
		
			throw e;
		
		}
	
	}

	assemble(SQLArr){

		let SQLStr = '';
		if(SQLArr.length > 0) {
		
			SQLStr = ' WHERE ';
			for(let i=0,c=SQLArr.length;i<c;i++) {

				SQLStr += SQLArr[i];
				if(i !== (c - 1)) SQLStr += ' AND ';

			}

		}
		return SQLStr;
		
	}

	getCityData(lO){

		return new Promise((resolve,reject)=>{

			try {

				let SQL = 'SELECT name, asciiname, alternatenames, lat, lon, pop, alt, tz FROm data_cities'
				let sqlWhere = [];
				let sqlLookup = [];
				let sortBy ='pop';
				let sortDir = 'DESC'
				let limit = 40;

				if(lO.countries) {
					sqlWhere.push('countrycode = ?');
					sqlLookup.push(lookupObj.countries);
				}

				if(lO.sortDir) sortDir = lO.sortDir;
				// lO.sortDir && ['ASC','DESC'].includes(lO.sortDir)

				if(lO.sortBy) sortBy = lO.sortBy;
				// && ['name','pop','alt'].includes(lO.sortBy)

				if(lO.limit) limit = lO.limit;

				let sqlStr = SQL + this.assemble(sqlWhere) + ' ORDER BY '+sortBy+ ' ' +  sortDir + ' LIMIT '+limit;
				this.App.DB.q(sqlStr,sqlLookup)
					.then(res=>resolve(res))
					.catch(e=>{
						
						throw e;
					
					});

			} catch(e) {

				reject(e);

			}


		});

	}

	getCountryData(lO){

		return new Promise((resolve,reject)=>{

			try {

				let SQL = 'SELECT countrycode, continent, name, lat, lon, capital FROm data_countries'
				let sqlWhere = [];
				let sqlLookup = [];
				let sortBy ='countrycode';
				let sortDir = 'DESC'
				let limit = 40;

				if(lO.sortDir) sortDir = lO.sortDir;
				if(lO.sortBy) sortBy = lO.sortBy;
				if(lO.limit) limit = lO.limit;

				let sqlStr = SQL + this.assemble(sqlWhere) + ' ORDER BY '+sortBy+ ' ' +  sortDir
				if(!lO.all) sqlStr += ' LIMIT '+limit;

				this.App.DB.q(sqlStr,sqlLookup)	
					.then(res=>resolve(res))
					.catch(e=>{
						
						throw e;
					
					});

			} catch(e) {

				reject(e);

			}


		});

	}

};