export default class Model {

	constructor(App){

		try {
		
			this.App = App;
			this.sql = `
				SELECT 
					ic.stub,ic.isDeleted,ic.isRemoved,
					ic.text, ic.text_original, ic.text_prev, ic.dt, ic.dt_edit,
					COALESCE (u.name,NULL) AS user_name,
					COALESCE(p.stub,NULL) as stub_parent,
					COALESCE(l.votes,0) as data_likes,
					COALESCE(r.reports,0) as data_reports,
					COALESCE(dc.comments,0) as data_comments,
					i.stub as item
				FROM \`item_comments\` AS ic
				INNER JOIN items i ON i.id = ic.item_id
				LEFT JOIN users u on u.id = ic.user_id AND u.active = 1 
				LEFT JOIN (SELECT COUNT(*) AS votes, comment_id FROM \`item_comment_likes\` WHERE \`liked\` = 1 GROUP BY comment_id) AS l ON l.comment_id = ic.id 
				LEFT JOIN (SELECT COUNT(*) AS reports, comment_id FROM \`item_comment_reports\` WHERE \`type\` > 0 GROUP BY comment_id) AS r ON r.comment_id = ic.id 
				LEFT JOIN (SELECT COUNT(*) AS comments, parent_id FROM \`item_comments\` WHERE parent_id IS NOT NULL GROUP BY parent_id) AS dc ON dc.parent_id = ic.id
				LEFT JOIN \`item_comments\` p ON p.id = ic.parent_id
			`;
			this.sqlUser = `
				SELECT 
					ic.stub,ic.isDeleted,ic.isRemoved,
					ic.text, ic.text_original, ic.text_prev, ic.dt, ic.dt_edit,
					COALESCE (u.name,NULL) AS user_name,
					COALESCE(l.votes,0) as data_likes,
					COALESCE(dc.comments,0) as data_comments,
					COALESCE(r.reports,0) as data_reports,
					COALESCE(p.stub,NULL) as stub_parent,
 					COALESCE(uL.liked,0) as user_liked,
 					COALESCE(uS.saved,0) as user_saved,
 					COALESCE(uR.type,0) as user_reported,
					i.stub as item 					
				FROM \`item_comments\` AS ic
				INNER JOIN items i ON i.id = ic.item_id
				LEFT JOIN users u on u.id = ic.user_id AND u.active = 1
				LEFT JOIN (SELECT COUNT(*) AS votes, comment_id FROM \`item_comment_likes\` WHERE \`liked\` = 1 GROUP BY comment_id) AS l ON l.comment_id = ic.id
				LEFT JOIN (SELECT COUNT(*) AS reports, comment_id FROM \`item_comment_reports\` WHERE \`type\` > 0 GROUP BY comment_id) AS r ON r.comment_id = ic.id
				LEFT JOIN (SELECT COUNT(*) AS comments, parent_id FROM \`item_comments\` WHERE parent_id IS NOT NULL GROUP BY parent_id) AS dc ON dc.parent_id = ic.id
				LEFT JOIN \`item_comments\` p ON p.id = ic.parent_id 
				LEFT JOIN \`item_comment_likes\` uL ON uL.comment_id = ic.id AND uL.user_id = ?
				LEFT JOIN \`item_comment_saves\` uS ON uS.comment_id = ic.id AND uS.user_id = ?
				LEFT JOIN \`item_comment_reports\` uR ON uR.comment_id = ic.id AND uR.user_id = ?
			`;

		} catch(e) {
		
			console.log(e);
		
		}
	
	}

	assemble(SQLArr){

		let SQLStr = '';
		if(SQLArr.length > 0) {
		
			SQLStr = ' WHERE ';
			for(let i=0,c=SQLArr.length;i<c;i++) {

				SQLStr += SQLArr[i];
				if(i !== (c - 1)) SQLStr += ' AND ';

			}

		}
		return SQLStr;
		
	}

	getComments(obj){


		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				let sqlStr = that.sql;
				let sqlWhereArr = [],
					sqlStrArr = [];

				if(obj.userId) {

					sqlStr = that.sqlUser;
					sqlWhereArr = [obj.userId,obj.userId,obj.userId];

				}

				if(obj.item) {
					sqlStrArr.push('i.stub = ?');
					sqlWhereArr.push(obj.item);
				}

				if(obj.upTo) {
					sqlStrArr.push('ic.id < (SELECT id FROM item_comments WHERE stub = ?)')
					sqlWhereArr.push(obj.upTo);
				}

				if(obj.parent) {
					sqlStrArr.push('ic.parent_id = (SELECT id FROM item_comments WHERE stub = ?)')
					sqlWhereArr.push(obj.parent);

				}

				if(obj.stub) {
					sqlStrArr.push('ic.stub = ?');
					sqlWhereArr.push(obj.stub);
				}

				if(obj.id) {
					sqlStrArr.push('ic.id = ?');
					sqlWhereArr.push(obj.id);
				}

				if(!(!!obj.stub || !!obj.id || !!obj.upTo || !!obj.parent)) sqlStrArr.push('ic.parent_id IS NULL');

				sqlStr += this.assemble(sqlStrArr);

				let hasSort = false;
				if(obj.sortBy){

					if(obj.sortBy === 'hot') {
						sqlStr += `
							ORDER BY data_likes DESC, ic.dt DESC
						`;
						hasSort = true;
					}

					if(obj.sortBy === 'new') {
						sqlStr += `
							ORDER BY ic.dt DESC;
						`;
						hasSort = true;
					}

					if(obj.sortBy === 'old') {
						sqlStr += `
							ORDER BY ic.dt ASC;
						`;
						hasSort = true;
					}

					if(obj.sortBy === 'comments') {
						sqlStr += `
							ORDER BY dc.comments DESC
						`;	
						hasSort = true;
					} 

				}
				if(!hasSort) sqlStr += `
						ORDER BY ic.dt DESC
					`;

				this.App.DB.q(sqlStr,sqlWhereArr)
					.then(comments=>{
						
						if(comments.length>0) {

							let uCl = require('./users').default;
							let ModelUsers = new uCl(this.App);

							let pList = [];
							comments.forEach((comment,cId)=>{

								comments[cId].user_owned = false;
								if(obj.userName && obj.userName === comment.user_name) comments[cId].user_owned = true;

								let cont = true;
								
								if(!comment.user_name) { 
									comment.user_name = '[deleted user]';
									cont = false;
								}

								if(comment.isRemoved) {
									comment.user_name = '[removed]';
									comment.text = comment.text_prev = comment.text_original = '[Removed By Moderator]';
									cont = false;
								}

								if(comment.isDeleted) {
									comment.user_name = '[deleted]';
									comment.text = comment.text_prev = comment.text_original = '[Deleted By User]';	
									cont = false;
								}


								if (cont) {

									pList.push(new Promise((rs,rj)=>{

										ModelUsers.getUserData({name:comment.user_name})
											.then(userData=>{

												if(userData.data_bans > 0) comments[cId].user_name = comments[cId].user_name + ' [banned]';

												comments[cId].user = userData;
												rs();

											})
											.catch(e=>{

												rj(e);

											});

									}));

								}

							});

							if(pList.length>0) {

								Promise.all(pList)
									.then(()=>{

										resolve(comments);

									})
									.catch(e=>{

										throw e;

									});

							} else {

								resolve(comments);

							}

						} else {

							resolve([]);

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	getItemId(stub) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				this.App.DB.q('SELECT id FROM items WHERE stub = ?',[stub])
					.then(results=>{

						if(results.length === 1) {

							resolve(results[0].id);

						} else {

							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	getCommentId(stub) {
		
		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				this.App.DB.q('SELECT id FROM item_comments WHERE stub = ?',[stub])
					.then(results=>{

						if(results.length === 1) {

							resolve(results[0].id);

						} else {

							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	getLocations(opts) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.App.DB.q(`

						SELECT 
							ic.\`text\`, 
							ic.stub as comment, 
							i.stub as item, 
							uloc.\`name\` AS user_name, 
							uloc.lat, 
							uloc.lon, 
							uloc.country_lat, 
							uloc.country_lon, 
							COALESCE(ua.stub,NULL) AS avatar 
						FROM item_comments AS ic
						INNER JOIN (
						  SELECT u.id, u.\`name\`, COALESCE(ul.latitude,NULL) AS lat, COALESCE(ul.longitude,NULL) AS lon, dco.lat AS country_lat, dco.lon AS country_lon 
						  FROM users AS u
						  LEFT JOIN user_locations ul ON ul.user_id = u.id 
						  INNER JOIN data_countries dco ON dco.countrycode = ul.countrycode
						  WHERE u.active = 1
						) uloc ON uloc.id = ic.user_id
					  	INNER JOIN items i ON i.id = ic.item_id						
						LEFT JOIN user_avatars ua ON ic.user_id = ua.user_id
						WHERE i.stub = ?
						GROUP BY ic.id

					`,[opts.item])
					.then(locationArr=>{

						resolve(locationArr);

					})
					.catch(e=>{

						throw e;

					});


			} catch(e) {

				reject(e);
			
			}

		});

	}

	setText(opts) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.App.DB.q('SELECT id,text FROM item_comments WHERE stub = ?',[opts.stub])
					.then(textIdObjArr=>{

						if(textIdObjArr.length === 1) {
							
							let commentId = textIdObjArr[0].id;
							let textPrev = textIdObjArr[0].text;
							that.App.DB.q('UPDATE item_commments SET text_prev = ?, text = ?, dt_edit = ? WHERE id = ?',[
									textPrev,
									opts.text,
									Math.round((new Date()).getTime()/1000),
									commentId
								])
								.then(results=>{

									resolve();

								})
								.catch(e=>{

									throw e;

								});

						} else {

							resolve('Comment not found');

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	insComment(insObj){

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.App.DB.q('INSERT INTO `item_comments` SET ?',insObj)
					.then(inserted=>{

						if(inserted.insertId > -1) {
							
							resolve(inserted.insertId);

						} else {

							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	setComment(opts) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				this.getItemId(opts.item)
					.then(itemId=>{

						that.App.stub('item_comments')
							.then(stub=>{

								let sqlIns = {
										item_id:itemId,
										user_id:opts.userId,
										dt:Math.floor((new Date).getTime()/1000),
										stub:stub,
										text:opts.text,
										text_prev:opts.text,
										text_original:opts.text,
									};

								if(opts.parent) {

									that.getCommentId(opts.parent)
										.then(parentId=>{

											sqlIns.parent_id = parentId;

											that.insComment(sqlIns)
												.then(insId=>{

													if(insId) {

														resolve(insId);

													} else {

														resolve();

													}

												})
												.catch(e=>{

													throw e;
												
												});


										})
										.catch(e=>{

											throw e;
										
										});

								} else {

									that.insComment(sqlIns)
										.then(insId=>{

											if(insId) {

												resolve(insId);

											} else {

												resolve();

											}

										})
										.catch(e=>{

											throw e;
										
										});

								}

							})
							.catch(e=>{

								throw e;

							});

					})
					.catch(e=>{

						throw e;

					});


			} catch(e) {

				reject(e);

			}

		});

	}

}