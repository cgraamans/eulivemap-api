export default class Model {

	constructor(DB){

		try {
		
			this.DB = DB;
		
		} catch(e) {
		
			console.log(e);
		
		}
	
	}

	assemble(SQLArr){

		let SQLStr = '';
		if(SQLArr.length > 0) {
		
			SQLStr = ' WHERE ';
			for(let i=0,c=SQLArr.length;i<c;i++) {

				SQLStr += SQLArr[i];
				if(i !== (c - 1)) SQLStr += ' AND ';

			}

		}
		return SQLStr;
		
	}

	fetch(opt,userId) {

		return new Promise((resolve,reject)=>{

			let SQLString = `
				SELECT 
					i.text, i.stub, i.link, i.author, i.img, i.dt, i.dt_created, i.dt_unixtime,
					s.name as source, s.stub as source_stub, s.logo as source_logo, s.country as source_country, s.language as source_language, s.type as source_type, s.url_twitter as source_twitter, s.url_rss as source_rss,
					u.name as source_user, 
					COALESCE(l.votes,0) as data_likes, 
					COALESCE(c.commented,0) as data_comments,
					COALESCE(r.reports,0) as data_reports
				FROM items as i 
				INNER JOIN sources as s ON s.id = i.source_id 
				LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM \`item_likes\` WHERE \`liked\` = 1 GROUP BY item_id) AS l ON l.item_id = i.id 
				LEFT JOIN (SELECT COUNT(*) AS reports, item_id FROM \`item_reports\` WHERE \`type\` > 0 GROUP BY item_id) AS r ON r.item_id = i.id 
				LEFT JOIN (SELECT COUNT(*) AS commented, item_id FROM \`item_comments\` WHERE isRemoved = 0 AND isDeleted = 0 GROUP BY item_id) AS c ON c.item_id = i.id
				LEFT JOIN users u ON u.id = i.user_id
			`;

			let SQLWhere = [];
			let SQLQueryVars = [];

			if(userId){

				SQLString = `
					SELECT 
						i.text, i.stub, i.link, i.author, i.img, i.dt, i.dt_created, i.dt_unixtime,
						s.name as source, s.stub as source_stub, s.logo as source_logo, s.country as source_country, s.language as source_language, s.type as source_type, s.url_twitter as source_twitter, s.url_rss as source_rss,
						u.name as source_user, 
						COALESCE(l.votes,0) as data_likes, 
						COALESCE(c.commented,0) as data_comments,
						COALESCE(r.reports,0) as data_reports,
						COALESCE(uL.liked,0) as user_liked,
						COALESCE(uS.saved,0) as user_saved,
						COALESCE(uR.type,0) as user_reported
					FROM items as i 
					INNER JOIN sources as s ON s.id = i.source_id 
					LEFT JOIN (SELECT COUNT(*) AS votes, item_id FROM \`item_likes\` WHERE \`liked\` = 1 GROUP BY item_id) AS l ON l.item_id = i.id 
					LEFT JOIN (SELECT COUNT(*) AS reports, item_id FROM \`item_reports\` WHERE \`type\` > 0 GROUP BY item_id) AS r ON r.item_id = i.id 
					LEFT JOIN (SELECT COUNT(*) AS commented, item_id FROM \`item_comments\` WHERE isRemoved = 0 AND isDeleted = 0 GROUP BY item_id) AS c ON c.item_id = i.id
					LEFT JOIN \`item_likes\` uL ON uL.item_id = i.id AND uL.user_id = ?
					LEFT JOIN \`item_saves\` uS ON uS.item_id = i.id AND uS.user_id = ?
					LEFT JOIN \`item_reports\` uR ON uR.item_id = i.id AND uS.user_id = ?
					LEFT JOIN users u ON u.id = i.user_id

				`;

				SQLQueryVars.push(userId);
				SQLQueryVars.push(userId);
				SQLQueryVars.push(userId);

			}

			//
			// assemble
			//
			
			// upTo
			if(opt.upTo) {
				SQLWhere.push('i.id < (SELECT id FROM items WHERE stub = ?)');
				SQLQueryVars.push(opt.upTo);
			}

			// from
			if(opt.from) {
				SQLWhere.push('i.dt_created > (SELECT dt_created FROM items WHERE stub = ?)');
				SQLQueryVars.push(opt.from);
			}

			// source
			if(opt.source) {
				SQLWhere.push('s.stub = ?');
				SQLQueryVars.push(opt.source);
			}

			// item
			if(opt.item) {
				SQLWhere.push('i.stub = ?');
				SQLQueryVars.push(opt.item);
			}

			// offset
			if(opt.offset){
				SQLWhere.push('i.dt_created > ?');
				SQLQueryVars.push(opt.offset);
			}

			// country
			if(opt.country) {
				SQLWhere.push('s.country = ?');
				SQLQueryVars.push(opt.country);
			}

			// language
			if(opt.language) {
				SQLWhere.push('s.language = ?');
				SQLQueryVars.push(opt.language);
			}

			// type
			if(opt.sourceType) {
				if (opt.sourceType === 'rss') SQLWhere.push('s.url_rss IS NOT NULL');
				if (opt.sourceType === 'twitter') SQLWhere.push('s.url_twitter IS NOT NULL');
				if (opt.sourceType === 'users') {
					SQLWhere.push('i.user_id IS NOT NULL');
				} else {
					SQLWhere.push('i.user_id IS NULL');
				}
			}

			// types
			if(opt.type){
				SQLWhere.push('s.type = ?');
				SQLQueryVars.push(opt.type);
			}


			// types
			if(opt.types && opt.types.length > 0){
				opt.types.forEach(type=>{
					SQLWhere.push('s.type = ?');
					SQLQueryVars.push(type);
				});
			}

			// stub
			if(opt.stub) {
				SQLWhere.push('i.stub = ?');
				SQLQueryVars.push(opt.stub);
			}

			//
			// final opt assemble
			//
			SQLString += this.assemble(SQLWhere);

			// order / sortBy
			let sortStr = ' ORDER BY data_likes DESC, dt_created DESC ';
			if(opt.sortBy){

				if(opt.sortBy === 'comments') sortStr = ' ORDER BY data_comments DESC, dt_created DESC ';
				if(opt.sortBy === 'old') sortStr = ' ORDER BY dt_created ASC ';
				if(opt.sortBy === 'new') sortStr = ' ORDER BY dt_created DESC ';

			}
			SQLString += sortStr;

			// limit
			if(opt.limit) {

				SQLString += ' LIMIT '+opt.limit;
			
			}

			this.DB.q(SQLString,SQLQueryVars)
				.then(items=>resolve(items))
				.catch(e=>reject(e));

		});

	}

}