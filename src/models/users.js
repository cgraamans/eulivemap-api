export default class Model {

	constructor(App){

		try {

			if(!App) throw 'NO APP';
			this.App = App;

		} catch(e) {

			throw e;

		}
	
	}

	assemble(SQLArr){

		let SQLStr = '';
		if(SQLArr.length > 0) {

			SQLStr = ' WHERE ';
			for(let i=0,c=SQLArr.length;i<c;i++) {

				SQLStr += SQLArr[i];
				if(i !== (c - 1)) SQLStr += ' AND ';

			}

		}
		return SQLStr;

	}

	getAvatarStub(userId) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.App.DB.q('SELECT stub FROM user_avatars WHERE user_id = ?',[userId])
					.then(subRes=>{

						if(subRes.length > 0) {
						
							resolve(subRes[0].stub);
						
						} else {

							that.App.stub('user_avatars')
								.then(ProtoStub=>{

									that.App.DB.q('INSERT INTO user_avatars SET ?',{

											user_id:userId,
											stub:ProtoStub

										})
										.then(()=>{

											resolve(ProtoStub);
										
										})
										.catch(e=>{

											reject(e);

										});

								})
								.catch(e=>{

									reject(e);

								});

						}

					})
					.catch(e=>{

						reject(e);
					
					});

			} catch(e) {

				reject(e);

			}

		});

	}

	getLocationData(opts) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				let SQLStr = `
					SELECT 
						
						ul.countrycode as country_code, 
						ul.latitude AS lat, 
						ul.longitude as lon, 
						ul.dt,
						
						COALESCE(dco.lat,NULL) as country_lat,
						COALESCE(dco.lon,NULL) as country_lon,
						COALESCE(dco.name,NULL) as country_name,
						COALESCE(dco.continent,NULL) as country_continent

					FROM user_locations AS ul
					LEFT JOIN data_countries dco ON dco.countrycode = ul.countrycode

				`;

				let sqlWhere = [],
					sqlLookup = [];

				if(opts.id) {
					sqlWhere.push('ul.user_id = ?')
					sqlLookup.push(opts.id);
				}

				if(opts.name) {
					sqlWhere.push('ul.user_id = (SELECT id FROM users WHERE name = ? AND active = 1 ORDER BY id LIMIT 1)')
					sqlLookup.push(opts.name);					
				}
				let finalSQL = SQLStr + this.assemble(sqlWhere);

				that.App.DB.q(finalSQL,sqlLookup)
					.then(res=>{
					
						if(res.length === 1) {

							resolve(res[0])							
						
						} else {

							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	getUserBadges(opts) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				let SQLStr = `
					SELECT 
						db.name,db.icon,db.color
					FROM user_badges AS ub
					INNER JOIN data_badges db ON db.id = ub.badge_id 

				`;

				let sqlWhere = ['ub.active = 1'],
					sqlLookup = [];

				if(opts.id) {
					sqlWhere.push('ub.user_id = ?')
					sqlLookup.push(opts.id);
				}

				if(opts.name) {
					sqlWhere.push('ub.user_id = (SELECT id FROM users WHERE name = ? AND active = 1 ORDER BY id LIMIT 1)')
					sqlLookup.push(opts.name);					
				}
				let finalSQL = SQLStr + this.assemble(sqlWhere);

				that.App.DB.q(finalSQL,sqlLookup)
					.then(res=>{

						resolve(res);

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

	getUserData(opts) {

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				let SQLStr = `
					SELECT 
						u.name, 
						COALESCE(ua.stub,NULL) as avatar, 
						COALESCE(uL.liked,0) as data_likes, 
						COALESCE(uC.comments,0) as data_comments, 
						COALESCE(uR.reports,0) as data_reports,
						COALESCE(ub.bans,0) as data_bans,
						u.profile, 
						u.dt_register
					FROM users AS u
					LEFT JOIN user_avatars ua ON ua.user_id = u.id
					LEFT JOIN (
						SELECT COUNT(*) AS bans, b.user_id 
						FROM user_bans AS b 
						GROUP BY b.user_id
					) as ub ON ub.user_id = u.id
					LEFT JOIN (
						SELECT COUNT(*) AS comments, ic.user_id 
						FROM item_comments AS ic 
						GROUP BY ic.user_id
					) as uC ON uC.user_id = u.id
					LEFT JOIN (
						SELECT COUNT(*) AS liked, ic.user_id 
						FROM item_comment_likes AS icl 
						INNER JOIN item_comments AS ic ON ic.id = icl.comment_id
						WHERE icl.liked = 1
						GROUP BY ic.user_id
					) AS uL ON uL.user_id = u.id
					LEFT JOIN (
						SELECT COUNT(*) AS reports, ic.user_id 
						FROM item_comment_reports AS icr 
						INNER JOIN item_comments AS ic ON ic.id = icr.comment_id
						GROUP BY ic.user_id
					) AS uR ON uR.user_id = u.id
				`;

				let sqlWhere = ['u.active = 1'];
				let sqlLookup = [];

				if(opts.name) {
					sqlWhere.push('u.name = ?');
					sqlLookup.push(opts.name);
				}
				if(opts.id) {
					sqlWhere.push('u.id = ?')
					sqlLookup.push(opts.id)	
				}

				let finalSQL = SQLStr + this.assemble(sqlWhere) + ' ORDER BY u.id DESC LIMIT 1';

				that.App.DB.q(finalSQL,sqlLookup)
					.then(subRes=>{
					
						if(subRes.length>0) {

							let userData = subRes[0];
							let pList = [

								new Promise((res,rej)=>{

									that.getLocationData(opts)
										.then(LRes=>{

											userData.location = LRes;
											res();

										})
										.catch(e=>{

											rej(e);

										});

								}),

								new Promise((res,rej)=>{

									that.getUserBadges(opts)
										.then(Lres=>{

											userData.badges = Lres;
											res();

										})
										.catch(e=>{

											rej(e);

										});
								}),

							];

							Promise.all(pList)
								.then(()=>{

									resolve(userData);

								})
								.catch(e=>{

									throw e;

								});
						
						} else {

							resolve();

						}

					})
					.catch(e=>{

						throw e;

					});

			} catch(e) {

				reject(e);

			}

		});

	}

};