export default class Model {

	constructor(DB){

		try {
		
			this.DB = DB;
		
		} catch(e) {
		
			console.log(e);
		
		}
	
	}

	assemble(SQLArr){

		let SQLStr = '';
		if(SQLArr.length > 0) {
		
			SQLStr = ' WHERE ';
			for(let i=0,c=SQLArr.length;i<c;i++) {

				SQLStr += SQLArr[i];
				if(i !== (c - 1)) SQLStr += ' AND ';

			}

		}
		return SQLStr;
		
	}

	fetch(opt,userId) {

		return new Promise((resolve,reject)=>{

			let SQLString = `
					SELECT 
						s.name, 
						s.stub, 
						s.logo, 
						s.type, 
						s.url_rss, 
						s.url_twitter, 
						s.country, 
						s.language,
						COALESCE(l.votes,0) as data_likes,
						COALESCE(c.commented,0) as data_comments
					FROM sources as s 
					LEFT JOIN (
							SELECT 
								COUNT(*) AS votes, 
								i.source_id 
							FROM \`item_likes\` AS il 
							INNER JOIN \`items\` AS i ON i.id = il.item_id 
							WHERE il.liked = 1 
							GROUP BY i.source_id
					) AS l ON l.source_id = s.id 
					LEFT JOIN (
							SELECT 
								COUNT(*) AS commented, 
								ii.source_id 
							FROM \`item_comments\` AS ic
							INNER JOIN \`items\` AS ii ON ii.id = ic.item_id
							WHERE 
								ic.isRemoved = 0 
								AND ic.isDeleted = 0 
								GROUP BY ii.source_id
					) AS c ON c.source_id = s.id
			`;

			let SQLWhere = [];
			let SQLQueryVars = [];

			if(userId){

				SQLString = `

					SELECT 
						s.name, 
						s.stub, 
						s.logo, 
						s.type, 
						s.url_rss, 
						s.url_twitter, 
						s.country, 
						s.language,
						COALESCE(l.votes,0) as data_likes,
						COALESCE(c.commented,0) as data_comments,
						COALESCE(sSav.saved,0) as user_saved
					FROM sources as s 
					LEFT JOIN (
							SELECT 
								COUNT(*) AS votes, 
								i.source_id 
							FROM \`item_likes\` AS il 
							INNER JOIN \`items\` AS i ON i.id = il.item_id 
							WHERE il.liked = 1 
							GROUP BY i.source_id
					) AS l ON l.source_id = s.id 
					LEFT JOIN (
							SELECT 
								COUNT(*) AS commented, 
								ii.source_id 
							FROM \`item_comments\` AS ic
							INNER JOIN \`items\` AS ii ON ii.id = ic.item_id
							WHERE 
								ic.isRemoved = 0 
								AND ic.isDeleted = 0 
								GROUP BY ii.source_id
					) AS c ON c.source_id = s.id
					LEFT JOIN source_saves sSav ON sSav.source_id = s.id AND sSav.user_id = ?

				`;

				SQLQueryVars.push(userId);
				// SQLQueryVars.push(userId);

			}

			//
			// assemble
			//


			// stub
			if(opt.stub) {
				SQLWhere.push('s.stub = ?');
				SQLQueryVars.push(opt.stub);
			}


			// sourceType
			if(opt.sourceType) {
				if (opt.sourceType === 'rss') SQLWhere.push('s.url_rss IS NOT NULL');
				if (opt.sourceType === 'twitter') SQLWhere.push('s.url_twitter IS NOT NULL');
			}

			// country
			if(opt.country) {
				SQLWhere.push('s.country = ?');
				SQLQueryVars.push(opt.country);
			}

			// language
			if(opt.language) {
				SQLWhere.push('s.language = ?');
				SQLQueryVars.push(opt.language);
			}

			// type
			if(opt.type){
				SQLWhere.push('s.type = ?');
				SQLQueryVars.push(opt.type);
			}

			// types
			if(opt.types && opt.types.length > 0){
				opt.types.forEach(type=>{
					SQLWhere.push('s.type = ?');
					SQLQueryVars.push(type);
				});
			}

			//
			// final opt assemble
			//
			SQLString += this.assemble(SQLWhere);

			// order / sortBy
			let sortStr = ' ORDER BY data_likes DESC';
			if(opt.sortBy){

				if(opt.sortBy === 'comments') sortStr = ' ORDER BY data_comments DESC';
				if(opt.sortBy === 'old') sortStr = ' ORDER BY dt_created ASC ';
				if(opt.sortBy === 'new') sortStr = ' ORDER BY dt_created DESC ';

			}
			SQLString += sortStr;

			// limit
			if(opt.limit) {

				SQLString += ' LIMIT '+opt.limit;
			
			}

			this.DB.q(SQLString,SQLQueryVars)
				.then(items=>resolve(items))
				.catch(e=>reject(e));

		});

	}

}