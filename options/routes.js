// Routing settings

// Switches
// 
// name			- string	- socket name
// controller 	- string 	- location of the controller
// noRoute		- boolean	- do not pass packet data from default to the user controller
// overwrite	- boolean	- overwrite packet data sent to the user controller from the default controller
// auth 		- boolean	- only allow for authenticated users. (default:false)
// level 		- int		- admin level required for controller (default 0);
//							  * if level is set, auth defaults to true

// ToDo: chain (string).

const ROUTES = [

	// overrides
	{
		name:"app.user.register",
		controller:"src/controllers/override.user.register"
	},

	// items
	{
		name:"items",
		controller:"src/controllers/items.js",
	},
	{
		name:"items.upTo",
		controller:"src/controllers/items.js",
	},
	{
		name:"items.from",
		controller:"src/controllers/items.js",
	},
	{
		name:"source.items",
		controller:"src/controllers/items.js",
	},
	{
		name:"user.items",
		controller:"src/controllers/items.js",
	},


	//  non-user controllers
	{
		name:"item",
		controller:"src/controllers/item.js",
	},
	{
		name:"item.latest",
		controller:"src/controllers/item.latest.js",
	},
	{
		name:"item.locations",
		controller:"src/controllers/item.locations.js",
	},

	// comments
	{
		name:"comments",
		controller:"src/controllers/comments.js",
	},
	{
		name:"comment.parent",
		controller:"src/controllers/comments.js",
	},

	{
		name:"comment.locations",
		controller:"src/controllers/comment.locations.js",
	},

	{
		name:"source",
		controller:"src/controllers/source.js",
	},

	{
		name:"data.get.countries",
		controller:"src/controllers/data.get.countries.js",
	},

	// user controllers
	{
		name:"user.get.data",
		controller:"src/controllers/user.get.data.js",
		auth:true
	},

	{
		name:"user.set.item.like",
		controller:"src/controllers/user.set.item.like.js",
		auth:true
	},
	{
		name:"user.set.item.report",
		controller:"src/controllers/user.set.item.report.js",
		auth:true
	},
	{
		name:"user.set.profile",
		controller:"src/controllers/user.set.profile.js",
		auth:true
	},

	{
		name:"user.set.comment",
		controller:"src/controllers/user.set.comment.js",
		auth:true
	},
	{
		name:"user.set.avatar",
		controller:"src/controllers/user.set.avatar.js",
		auth:true
	},
	{
		name:"user.set.location",
		controller:"src/controllers/user.set.location.js",
		auth:true
	},
	{
		name:"user.set.comment.like",
		controller:"src/controllers/user.set.comment.like.js",
		auth:true
	},
	{
		name:"user.set.comment.report",
		controller:"src/controllers/user.set.comment.report.js",
		auth:true
	},
	{
		name:"user.set.comment.delete",
		controller:"src/controllers/user.set.comment.delete.js",
		auth:true
	},
	{
		name:"mod.set.comment.remove",
		controller:"src/controllers/mod.set.comment.remove.js",
		auth:true,
		level:1
	},
	{
		name:"mod.set.comment.ban",
		controller:"src/controllers/mod.set.comment.ban.js",
		auth:true,
		level:2
	},

];
export default ROUTES;