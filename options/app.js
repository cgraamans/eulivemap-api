const APP = {

	// LOGIN/LOGOUT/TOKEN TIMES
	SessionTimeout: 3600, //s
	TokenTTL: 100, //s,

	maxUserNameLength:24,

	commentIntervalMax:3000, // maximum user's comment interval //ms
	commentLengthMax:1000,
	// OWASP PASSWORD CHECKER VARIABLES
	OWASP:{
		allowPassphrases       : true,
		maxLength              : 64,
		minLength              : 5,
		minPhraseLength        : 30,
		minOptionalTestsToPass : 2,
	},

	STATE:{

		refresh:3600000, //ms

	}

};
export default APP;